import { Component } from '@angular/core';

@Component({
  selector: "dubmate",
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent {
  selectedRoomId;
  state = 'room-list';

  showRoomDetail(roomId){
    this.selectedRoomId = roomId;
    this.state = 'room-detail';
  }

}
