import { Component, EventEmitter, Output } from '@angular/core';
import { RoomService } from './room.service';

@Component({
  selector: 'room-list',
  templateUrl: 'room-list.component.html'
})
export class RoomListComponent {
  @Output() roomSelect = new EventEmitter();
  rooms;
  
  constructor(roomService: RoomService){
    this.rooms = roomService.getRooms();
  }
}
