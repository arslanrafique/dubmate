import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { RoomService } from './room.service';
@Component({
  selector: 'room-detail',
  templateUrl: 'room-detail.component.html'
})
export class RoomDetailComponent implements OnInit, OnDestroy {

  room;
  paramsSubscription: Subscription;

  constructor(private route: ActivatedRoute,
              private roomService: RoomService) { }

  ngOnInit() {
      this.paramsSubscription = this.route.params.subscribe( params => { 
      this.room = this.roomService.getRoom(params['roomId']);
    });
  }

  ngOnDestroy() {
    this.paramsSubscription.unsubscribe();
  }

}
