import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { routing} from './app.routing';
import { MaterialModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FlexLayoutModule } from '@angular/flex-layout/flexbox';

import { AppComponent } from './app.component';
import { MainPage } from './mainpage/mainpage.component';
import { RoomService } from './room/room.service';
import { RoomListComponent } from './room/room-list.component';
import { RoomDetailComponent } from './room/room-detail.component';
import { TopMenu } from './topmenu/topmenu.component';
import { TopFilter } from './topfilter/topfilter.component';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    FlexLayoutModule,
    routing
    ],
  declarations: [
    AppComponent,
    MainPage,
    RoomListComponent,
    RoomDetailComponent,
    TopMenu,
    TopFilter
    ],
  providers: [
    RoomService,
    {provide: LocationStrategy, useClass: HashLocationStrategy}
   ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
