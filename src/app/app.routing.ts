import { RouterModule } from '@angular/router';
import { MainPage } from './mainpage/mainpage.component';
import { RoomListComponent} from './room/room-list.component';
import { RoomDetailComponent} from './room/room-detail.component';

export const routing = RouterModule.forRoot([
  {
    path: '',
    component: MainPage
  },
  {
    path: 'rooms',
    component: RoomListComponent
  },
  // {
  //   path: '',
  //   redirectTo: '/rooms',
  //   pathMatch: 'full'
  // },
  {
    path: 'rooms/:roomId',
    component: RoomDetailComponent
  },
]);
